# CPP-RSA

This project is a combination of both:
 * A C++ library implementing 64bit RSA ciphering and deciphering algorithms (with file IO support for keys).
 * A C++ executable serving as an immediate interface.

The library was ported to C++ from the C  [RSA-Library](https://github.com/andrewkiluk/RSA-Library/) of andrewkiluk.

In addition to the C++ port, improvement were made on the random key generation (with the C++11 standards), 
and optimisations (mostly on memory management / copy avoidance) were implemented. 

Technical documentation automatically generated for the library (and, to a lesser extent, for the program) can be found [here](https://axelpaccalin.gitlab.io/cpp-rsa/). It can also be generated locally using `doxygen`.



## Install 


#### System-wide

(Only available to debian based distributions with amd64 or i686 architectures).
 * Get the `.deb` package from the latest release relevant to your architecture in [release page](https://gitlab.com/AxelPaccalin/cpp-rsa/-/releases).
 * Install with your package installer.
 
Example with `apt` under an `amd64` architecture:
 
`sudo apt install ./cpp-rsa_amd64.deb`


#### Local

(Only available to Linux based distributions or Windows, both with either amd64 or i686 architectures)
* Get the `build_artifacts` archive from the latest release relevant to your architecture and OS in the [release page](https://gitlab.com/AxelPaccalin/cpp-rsa/-/releases).



## Compile

This project only requires a C++ compiler with support for the `C++17` standards. You can install it under a debian-based distribution with `sudo apt install build-essentials`.

A cmake build configuration is provided but will also require `cmake` to be installed. You can install it under a debian-based distribution with `sudo apt install cmake`.

```shell script
mkdir build && cd build
cmake .. -DEXAMPLE=OFF
make
```

The __optional__ flag `-DEXAMPLE=OFF` will disable the compilation of examples (enabled by default). 

Another __optional__ flag `-DTEST=ON` can enable the build of unit tests (disabled by default). Unit testing require the boost libraries.  You can install it under a debian-based distribution with `sudo apt install libboost-all-dev`.

Once compiled with make, unit tests can be run with `make test`.



## Usage

The following examples are related to the program. For an example of the library usage, read [examples/CipherExample.cpp](examples/CipherExample.cpp). In this case, you may also want to take a look at the program source code  [src/main.cpp](src/main.cpp).

This usage example assumes a system-wise install. Otherwise, replace `cpp-rsa` by `/path/to/exe.ext` (path can be relative, there will be no file extension on linux) in all the following commands.


#### Key generation

```shell script
# Generates a new random pair of keys in files named 'keyR.*'.
cpp-rsa generate keyR
# Generates a new pair of keys with the seed 42 in files named 'key42.*'.
cpp-rsa generate key42 -s 42
```


#### Console oriented usage:

Cypher a message with our key42 public key:
```shell script
cpp-rsa cipher -k key42.rsa-pub
#< Hello world !
#> 0x3d7b9d85 0x349aae37 0x42cd77fd 0x42cd77fd 0x2e99c374 0xa46f4b5 0x322acf08 0x2e99c374 0xa707d99 0x42cd77fd 0x43fe143d 0xa46f4b5 0x279e927b 0x0
```
Decipher this message with our key42 private key:
```shell script
cpp-rsa decipher -k key42.rsa-priv
#< 0x3d7b9d85 0x349aae37 0x42cd77fd 0x42cd77fd 0x2e99c374 0xa46f4b5 0x322acf08 0x2e99c374 0xa707d99 0x42cd77fd 0x43fe143d 0xa46f4b5 0x279e927b 0x0
#> Hello world !
```

Using pipes (possibly with sub-processes or sockets):
```shell script
echo "This message was ciphered and deciphered !" | cpp-rsa cipher -k key42.rsa-pub | cpp-rsa decipher -k key42.rsa-priv
#> This message was ciphered and deciphered !
```


#### File oriented usage:

```shell script
# Create a file 'src.txt' with a message to cipher.
echo "This message was ciphered and deciphered !" >> src.txt

# Cipher this file with our key42 public key, and store the result in 'data.rsa'.
cpp-rsa cipher src.txt -k key42.rsa-pub -o data.rsa

# Decipher this message with our key42 private key, and store the result in 'dst.txt'.
cpp-rsa decipher data.rsa -k key42.rsa-priv -o dst.txt
```