/** @file    Tools.cpp
 *  @authors Axel Paccalin.
 *  @brief   Random message generation for test cases.
 * */

#ifndef CPP_RSA_TEST_TOOLS_HPP
#define CPP_RSA_TEST_TOOLS_HPP

#include "../include/cpp-rsa/Message.hpp"

namespace CPP_RSA::Test{
    /// Generates a random string of length 'len'.
    std::string randomString(const size_t &len) {
        // Pool of available chars.
        static const char alphanum[] =
                " -_,.;:?!"
                "0123456789"
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                "abcdefghijklmnopqrstuvwxyz";

        // Seed the RNG.
        srand( (unsigned) time(NULL) * getpid());

        // Prepare the output buffer.
        std::string tmp;
        tmp.reserve(len);

        // Add random char one by one, until we've added `len` of them.
        for (int _ = 0; _ < len; ++_) tmp += alphanum[rand() % (sizeof(alphanum) - 1)];

        // Return the buffer.
        return tmp;
    }

    /// Generates a random clear message of length 'len'.
    Message<Clear> randomMessage(const size_t &len){
        // Return a message, built from a random string of appropriate size.
        return Message<Clear>(randomString(len).data(), len);
    }
}

#endif //CPP_RSA_TEST_TOOLS_HPP
