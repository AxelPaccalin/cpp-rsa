/** @file    KeyIOTest.cpp
 *  @authors Axel Paccalin
 *  @brief   Test cases for Key (KeyPair) <-> file (pair of files) saving / loading operations.
 * */



#define BOOST_TEST_MODULE KeyIOTest

#include <boost/test/unit_test.hpp>

#include "../include/cpp-rsa/Key.hpp"

using std::string;

namespace CPP_RSA::Test {
    /// Ensure that a key loaded from the file into which it was just saved matches with the original.
    BOOST_AUTO_TEST_CASE(SingleKey_SaveLoad) {
        // Create a "key".
        auto k = Key(55555, 66666);
        // Save it.
        k.save("test.fake_key");
        // Load it into a copy.
        auto kCopy = Key::load("test.fake_key");

        // Compare both.
        BOOST_CHECK_EQUAL(kCopy.getModulus(), k.getModulus());
        BOOST_CHECK_EQUAL(kCopy.getExponent(), k.getExponent());
    }

    /// Ensure that a KeyPair loaded from the file template into which it was just saved matches with the original.
    BOOST_AUTO_TEST_CASE(KeyPair_SaveLoad) {
        // Used to obtain a seed for the RNG.
        std::random_device rd;
        // RNG: Standard Mersenne twister engine.
        std::mt19937 rng;
        // Generate an initial set of random keys to compare later.
        const auto kP = KeyPair::generate(rng);
        // Save them.
        kP.save("test_pair");
        // Load them into a copy.
        auto kPCopy = KeyPair::load("test_pair");

        // Compare both.
        BOOST_CHECK_EQUAL(kPCopy.pri.getModulus(), kP.pri.getModulus());
        BOOST_CHECK_EQUAL(kPCopy.pri.getExponent(), kP.pri.getExponent());
        BOOST_CHECK_EQUAL(kPCopy.pub.getModulus(), kP.pub.getModulus());
        BOOST_CHECK_EQUAL(kPCopy.pub.getExponent(), kP.pub.getExponent());
    }
}
