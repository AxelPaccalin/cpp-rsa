/** @file    CipherExample.cpp
 *  @authors Axel Paccalin
 *  @brief   Simple example of the cpp-rsa library usage.
 * */

#include <iostream>
#include "Key.hpp"

using std::string;
using std::cout, std::endl;

using namespace CPP_RSA;

static const string SECRET_DATA = "Hello World !";

int main(){
    /// Speculative use case: A receiver (Bob) want to receive messages from any potential sender (Alice),
    ///                       without anyone else (Eve) being capable of knowing the content of these messages.

    /// Bob's side
    // Create a new pair of keys.
    std::random_device rd;             // Used to obtain a seed for the RNG.
    std::mt19937 rng(rd());            // RNG: Standard Mersenne twister engine (can be instantiated with fixed seed, but only for debug purposes).
    auto kp = KeyPair::generate(rng);  // Generate the KeyPair.

    // Send the public key in a clear (non-ciphered) way to any potential sender (Alice and Eve) (not done here).
    // Remember that Bob is the only one knowing the value of his private key (as he didn't send it to anybody and will never do so).


    /// Alice's side.
    // Receive the public key of Bob (not done here).

    // Instantiate the clear message.
    const Message<Clear> src(SECRET_DATA.data(), SECRET_DATA.length()+1);
    // Create a buffer for the corresponding ciphered message.
    Message<Ciphered> ciphered;
    // Cypher the message into the buffer using the public key of the Bob.
    kp.pub.cipher(src, ciphered);

    // Send this ciphered message to the Bob (not done here).


    /// Bob's side
    // Receive the ciphered message from Alice (not done here).

    // Create a buffer for the corresponding deciphered message.
    Message<Clear> dst;
    // Decipher the message using our (Bob's) private key.
    kp.pri.decipher(ciphered, dst);


    /// Eve's side
    // Receive the public key of Bob                    (not done here).
    // Intercept the ciphered message from Alice to Bob (not done here).

    // Create a buffer for the corresponding deciphered message.
    Message<Clear> invalidDst;
    // Attempt to decipher the message using Bob's public key (the one used for cyphering by Alice, and the only one available to anybody but Bob).
    kp.pub.decipher(ciphered, invalidDst);



    /// End of the speculative use case.
    /// We can now print some data to demonstrate the result:
    cout << "Alice  : " << src.data << endl;
    cout << "Public : ";
    for (size_t i = 0; i < ciphered.size; ++i) cout << "0x" << std::hex << ciphered.data[i] << " ";
    cout << endl;
    cout << "Bob    : " << dst.data << endl;
    cout << "Eve    : " << invalidDst.data << endl;
}