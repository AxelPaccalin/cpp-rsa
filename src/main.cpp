/** @file    main.cpp
 *  @authors Axel Paccalin
 *  @brief   Program acting as a direct command-line interface to the cpp-rsa library.
 *  @details This program can be used to generate keys, cipher and decipher messages.
 *           Data can be provided / outputted either with binary files, or through stdio.
 * */

#include <optional>
#include <sstream>
#include <list>
#include <iostream>
#include <fstream>
#include <algorithm>

#include "Key.hpp"

// Containers.
using std::optional;
using std::string, std::stringstream, std::istringstream;
using std::list;
// Cast
using std::hex;
// IO.
using std::cin, std::cout, std::cerr, std::endl;
using std::ifstream, std::ofstream, std::ios;
// Algorithms.
using std::to_string, std::transform;
// Random.
using std::random_device, std::mt19937;
// Errors.
using std::runtime_error;
// Lib cpp-rsa
using namespace CPP_RSA;

/// Program argument parser / container.
class ProgArgs{
private:
    bool usageFlag, verboseFlag;
    optional<string> command, keyPath, outputPath, seed;

    list<string> sources;
public:
    ProgArgs(int argc, char *argv[])
            : usageFlag(false), verboseFlag(false), command({}),
              keyPath({}), outputPath({}), seed({}) {
        for (int i = 1; i < argc; ++i) {
            string arg = argv[i];
            if ((arg == "-h") || (arg == "--help"))
                usageFlag = true;
            else if ((arg == "-v") || (arg == "--verbose"))
                verboseFlag = true;
            else if ((arg == "-o") || (arg == "--output"))
                outputPath = argv[++i];
            else if ((arg == "-k") || (arg == "--key"))
                keyPath = argv[++i];
            else if ((arg == "-s") || (arg == "--seed"))
                seed = argv[++i];
            else if(!command.has_value())
                command = string(argv[i]);
            else
                sources.emplace_back(argv[i]);
        }
        if(command.has_value()){
            string &cmd = command.value();
            transform(cmd.begin(), cmd.end(), cmd.begin(), tolower);
        }
    }

    const bool &usage() const {
        return usageFlag;
    }
    const bool &verbose() const {
        return verboseFlag;
    }

    const optional<string> &getCommand() const {
        return command;
    }

    const optional<string> &getKeyPath() const {
        return keyPath;
    }

    const optional<string> &getOPath() const {
        return outputPath;
    }
    const optional<string> &getSeed() const {
        return seed;
    }

    const list<string> &getSources() const {
        return sources;
    }
};

/// Computes the size (in bytes) of a file.
int fileSize(const string &path){
    ifstream i(path, ios::binary);
    if (!i) throw runtime_error((string)"Could not open the input file '" + path + "'.");
    i.seekg(0,ios::end);
    int size = i.tellg();
    i.close();
    return size;
}
/// Save a byte array into a file.
void saveMessage(const string &path, const char *msg, const size_t &size){
    ofstream o(path, ios::out | ios::binary);
    if (!o)  throw runtime_error((string)"Could not open the output file '" + path + "'.");
    o.write(msg, size);
}
/// Load a byte array from a file.
char *loadMessage(const string &path, size_t &size){
    size = fileSize(path);
    char *buffer = new char[size];
    ifstream i(path, ios::in | ios::binary);
    if (!i) {
        delete[] buffer;
        throw runtime_error((string)"Could not open the output file '" + path + "'.");
    }

    i.read(buffer, size);
    return buffer;
}

/// Load a clear message from a file.
Message<Clear> loadClearMessage(const string &path){
    size_t size;
    char *msg = loadMessage(path, size);
    auto res = Message<Clear>(msg, size);
    delete[] msg;
    return res;
}
/// Load a ciphered message from a file.
Message<Ciphered> loadCipheredMessage(const string &path){
    size_t size;
    char *msg = loadMessage(path, size);
    if(size % 8 != 0)
        throw runtime_error("Invalid size for ciphered message");

    auto res = Message<Ciphered>((long long *) msg, size / 8);
    delete[] msg;
    return res;
}

/// Read a whole input line from stdin (console / bash|pipe) as a stringstream.
void getFullInput(stringstream &input){
    string in;
    getline(cin, in);
    input << in;
}

/// Read a clear message from stdin.
Message<Clear> inputClearMessage(const bool &verbose){
    if (verbose) cout << "Write clear message:" << endl;
    stringstream input;
    getFullInput(input);
    const string msg = input.str();
    return Message<Clear>(msg.data(), msg.length()+1);
}
/// Read a ciphered message from stdin.
Message<Ciphered> inputCipheredMessage(const bool &verbose){
    if (verbose) cout << "Write ciphered message:" << endl;
    stringstream inputStream;
    getFullInput(inputStream);
    list<Ciphered> cipheredCharacters;
    string hexFragment;

    while (inputStream >> hexFragment){
        hexFragment.erase(0, 2);
        Ciphered cipheredChar;
        istringstream(hexFragment) >> hex >> cipheredChar;
        cipheredCharacters.push_back(cipheredChar);
    }

    auto res = Message<Ciphered>(cipheredCharacters.size());

    Ciphered *resIt = res.data;
    for (const auto &c : cipheredCharacters){
        (*resIt) = c;
        resIt ++;
    }

    return res;
}

/// Demonstrate the usage of the library.
int runUsage(const ProgArgs &args){
    if(!args.getCommand().has_value())
        cout << "Usage: cpp-rsa <COMMAND> ..." << endl
             << "  Commands:" << endl
             << "    gen, generate      Generates a new key pair." << endl
             << "    cipher             Ciphers a message."        << endl
             << "    decipher           Deciphers a message."      << endl
             << "For command specific help, see: cpp-rsa <command> -h" << endl;

    else if(args.getCommand().value() == "generate" || args.getCommand().value() == "gen")
        cout << "Usage: cpp-rsa " << args.getCommand().value() << " [-OPTION...] <key_pair_name>"          << endl
             << "  key_pair_name: Required (path) filename for both files in which the keys will be saved" << endl
             << "  Options: "                                                                              << endl
             << "    -s, --seed <number>    Optional seed (integer) used for key generation."              << endl;

    else if(args.getCommand().value() == "cipher")
        cout << "Usage: cpp-rsa " << args.getCommand().value() << " [-OPTION...] [input_file]"                                                             << endl
             << "  input_file: Optional (path) file.ext containing the data to cipher (if not provided, will use later console input)."                    << endl
             << "  Options: "                                                                                                                              << endl
             << "    -k, --key     <file>   Required (path) file.ext of the key to use for ciphering."                                                     << endl
             << "    -o, --output  <file>   Optional (path) file.ext into which to save the ciphered data (if not provided, will dump as console output)." << endl
             << "    -v, --verbose          Optional (flag) provide cues to the user when his input is required (disabled by default)."                    << endl;

    else if(args.getCommand().value() == "decipher")
        cout << "Usage: cpp-rsa " << args.getCommand().value() << " [-OPTION...] [input_file]"                                                               << endl
             << "  input_file: Optional (path) file.ext containing the data to decipher (if not provided, will use later console input)."                    << endl
             << "  Options: "                                                                                                                                << endl
             << "    -k, --key     <file>   Required (path) file.ext of the key to use for deciphering."                                                     << endl
             << "    -o, --output  <file>   Optional (path) file.ext into which to save the deciphered data (if not provided, will dump as console output)." << endl
             << "    -v, --verbose          Optional (flag) provide cues to the user when his input is required (disabled by default)."                      << endl;

    else{
        cerr << "Unknown command `" << args.getCommand().value() << "`. Use -h or --help for help" << endl;
        return 1;
    }
    return 0;
}
/// Generate key pairs.
int runKeyGeneration(const ProgArgs &args){
    if (args.getSources().empty()){
        cerr << "Missing required key destination argument. Use -h or --help for help" << endl;
        return -1;
    } else if (args.getSources().size() != 1) {
        cerr << "Too many arguments for the key generation commend. Use -h or --help for help" << endl;
        return -1;
    }

    unsigned int seed;
    if(args.getSeed().has_value()){
        // Parse the user-defined argument.
        seed = stol(args.getSeed().value());
        cout << "Using '" + to_string(seed) + "' as seed." << endl;
    } else {
        // Used to obtain a seed for the RNG.
        random_device rd;
        // Generate random seed.
        seed = rd();
        cout << "No seed provided. Using random seed '" + to_string(seed) + "'." << endl;
    }

    // RNG: Standard Mersenne twister engine.
    mt19937 rng(seed);

    // Generate the key pair.
    KeyPair kp = KeyPair::generate(rng);

    // Save the pair to the target path.
    kp.save(args.getSources().front());
    cout << "Key generated and saved successfully" << endl;

    return 0;
}
/// Cipher messages.
int runMsgCipher(const ProgArgs &args){
    // Make sure there is no unexpected argument.
    if (args.getSources().size() > 1) {
        cerr << "Too many arguments for the message cipher command. Use -h or --help for help" << endl;
        return -1;
    }

    // Ensure a path to a ciphering key was provided.
    if (!args.getKeyPath().has_value()){
        cerr << "Error: Missing required -k or --key argument. Use -h or --help for help." << endl;
        return -1;
    }
    // Then load that ciphering key into memory from the provided path.
    const auto key = Key::load(args.getKeyPath().value());

    // If no source parameter was supplied, input the message to cipher (from stdin: keyboard / pipe).
    // Otherwise, load it from the specified source.
    const auto clear = args.getSources().empty()
                       ? inputClearMessage(args.verbose())
                       : loadClearMessage(args.getSources().front());

    // Create a container to store the ciphered result.
    Message<Ciphered> ciphered;
    key.cipher(clear, ciphered);

    // If an output path is defined, save the ciphered message there.
    if (args.getOPath().has_value()){
        saveMessage(args.getOPath().value(), (char *)ciphered.data, ciphered.size * 8);
        cout << "Message ciphered and saved successfully" << endl;
    }
    // Otherwise, dump it in the console as space separated hexadecimal values.
    else{
        for (size_t i = 0; i < ciphered.size; ++i) cout << "0x" << hex << ciphered.data[i] << " ";
        cout << endl;
    }

    return 0;
}
/// Decipher messages.
int runMsgDecipher(const ProgArgs &args){
    // Make sure there is no unexpected argument.
    if (args.getSources().size() > 1) {
        cerr << "Too many arguments for the message decipher command. Use -h or --help for help" << endl;
        return -1;
    }

    // Ensure a path to a ciphering key was provided.
    if (!args.getKeyPath().has_value()){
        cerr << "Error: Missing required -k or --key argument. Use -h or --help for help." << endl;
        return -1;
    }
    // Then load that ciphering key into memory from the provided path.
    const auto key = Key::load(args.getKeyPath().value());

    // If no source parameter was supplied, input the message to decipher (from stdin: keyboard / pipe).
    // Otherwise, load it from the specified source.
    const auto ciphered = args.getSources().empty()
                          ? inputCipheredMessage(args.verbose())
                          : loadCipheredMessage(args.getSources().front());

    Message<Clear> clear;
    key.decipher(ciphered, clear);


    // If an output path is defined, save the deciphered message there.
    if (args.getOPath().has_value()){
        saveMessage(args.getOPath().value(), clear.data, ciphered.size);
        cout << "Message deciphered and saved successfully" << endl;
    }
    // Otherwise, dump it in the console as ASCII text.
    else cout << clear.data << endl;

    return 0;
}

/// Program entry point.
int main(int argc, char *argv[]){
    // Parse the arguments.
    auto args = ProgArgs(argc, argv);

    // If the user asks for help, provide it and stop there.
    if (args.usage()) return runUsage(args);

    // Ensure that the user provided a command.
    if (!args.getCommand().has_value()){
        cerr << "Missing command argument. Use -h or --help for help" << endl;
        return -1;
    }

    // Handle the user command.
    if (args.getCommand().value() == "generate" || args.getCommand().value() == "gen") return runKeyGeneration(args);
    if (args.getCommand().value() == "cipher")                                         return runMsgCipher(args);
    if (args.getCommand().value() == "decipher")                                       return runMsgDecipher(args);

    // Implicit else, the command doesn't match any expected command name.
    cerr << "Unknown command `" << args.getCommand().value() << "`. Use -h or --help for help" << endl;
    return -1;
};