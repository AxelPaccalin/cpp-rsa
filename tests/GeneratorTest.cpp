/** @file    CipherTest.cpp
 *  @authors Axel Paccalin
 *  @brief   Test cases for the random KeyPair generator behavior.
 *  @details The validity of the generated KeyPair is tested in `CipherTest.cpp`.
 * */

// The validity of the generated key pairs is tested in `CipherTest.cpp`.
// This file only contains test cases for the random generator behavior.

#define BOOST_TEST_MODULE GeneratorTest

#include <boost/test/unit_test.hpp>

#include "../include/cpp-rsa/Key.hpp"
#include "Tools.hpp"

using std::string;

namespace CPP_RSA::Test {
    /// Ensure that we can debug easily by fixing the RNG.
    BOOST_AUTO_TEST_CASE(SeedBasedDeterminism) {
        // RNG: Standard Mersenne twister engine.
        std::mt19937 rng;
        // Generate a "random" (fixed by seeds) set of keys to run the test with.
        rng.seed(42);
        const auto keys1 = KeyPair::generate(rng);
        rng.seed(1);
        const auto keys2 = KeyPair::generate(rng);
        rng.seed(100);
        const auto keys3 = KeyPair::generate(rng);
        rng.seed(42);
        const auto keys4 = KeyPair::generate(rng);
        rng.seed(1);
        const auto keys5 = KeyPair::generate(rng);
        rng.seed(100);
        const auto keys6 = KeyPair::generate(rng);

        // Verify that pairs with the same seed match.
        BOOST_CHECK_EQUAL(keys1.pri.getModulus(), keys4.pri.getModulus());
        BOOST_CHECK_EQUAL(keys1.pri.getExponent(), keys4.pri.getExponent());
        BOOST_CHECK_EQUAL(keys2.pri.getModulus(), keys5.pri.getModulus());
        BOOST_CHECK_EQUAL(keys2.pri.getExponent(), keys5.pri.getExponent());
        BOOST_CHECK_EQUAL(keys3.pri.getModulus(), keys6.pri.getModulus());
        BOOST_CHECK_EQUAL(keys3.pri.getExponent(), keys6.pri.getExponent());
    }

    /// Ensure that the RNG does matter (random keys being generated).
    BOOST_AUTO_TEST_CASE(KeyRandomness) {
        // Used to obtain a seed for the RNG.
        std::random_device rd;
        // RNG: Standard Mersenne twister engine.
        std::mt19937 rng;

        // Generate an initial set of random keys to compare later.
        const auto keysBase = KeyPair::generate(rng);

        // Extremely excessive value for the watchdog.
        // But we do not want the whole CI pipelines to fail because of a very rare random streak invalidating the test.
        unsigned int watchdog = 5;
        while (watchdog > 0){
            const auto newKeys = KeyPair::generate(rng);
            // Since the public key have to "match" anyway:
            // Only comparing the private key should be enough to test for a difference.
            if(newKeys.pri.getExponent() != keysBase.pri.getExponent()
            || newKeys.pri.getModulus() != keysBase.pri.getModulus())
                break;
            // If the new key is the same as the old, attempt again.
            --watchdog;
        }

        // Make sure we didn't generate the same key 5 consecutive times.
        BOOST_CHECK_NE(watchdog, 0);
    }
}
