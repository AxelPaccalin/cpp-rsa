# @Authors: Axel Paccalin,
# @Brief: CMake toolchain to cross-compile for Windows x86_64 targets from Linux x86_64 host.

# Processor architecture of the host.
SET(CMAKE_HOST_SYSTEM_PROCESSOR x86_64)

# Operating system of the target .
SET(CMAKE_SYSTEM_NAME Windows)
# Processor architecture of the target
SET(CMAKE_SYSTEM_PROCESSOR x86_64)

# which compilers to use for C and C++.
SET(CMAKE_C_COMPILER x86_64-w64-mingw32-gcc)
SET(CMAKE_CXX_COMPILER x86_64-w64-mingw32-g++)
SET(CMAKE_RC_COMPILER x86_64-w64-mingw32-windres)

# Location of the target environment.
SET(CMAKE_FIND_ROOT_PATH  x86_64-w64-mingw32)

# adjust the default behaviour of the FIND_XXX() commands:
# search headers and libraries in the target environment, search
# programs in the host environment.
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
