# @Authors: Axel Paccalin,
# @Brief: CMake toolchain to cross-compile for Linux i686 targets from Linux x86_64 host.

# Processor architecture of the host
SET(CMAKE_HOST_SYSTEM_PROCESSOR x86_64)

# the name of the target operating system
SET(CMAKE_SYSTEM_NAME Linux)
# Processor architecture of the target
SET(CMAKE_SYSTEM_PROCESSOR x86_64)

# Compiler flags (64bit)
SET(CMAKE_C_FLAGS -m32 CACHE STRING "" FORCE)
SET(CMAKE_CXX_FLAGS -m32 CACHE STRING "" FORCE)
SET(CMAKE_RC_FLAGS -m32 CACHE STRING "" FORCE)