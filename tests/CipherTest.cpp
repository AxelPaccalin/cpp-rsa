/** @file    CipherTest.cpp
 *  @authors Axel Paccalin
 *  @brief   Test cases for the validity of generated KeyPair.
 * */


#define BOOST_TEST_MODULE BoolTest

#include <boost/test/unit_test.hpp>

#include "../include/cpp-rsa/Key.hpp"
#include "Tools.hpp"

using std::string;

namespace CPP_RSA::Test {
    /// Ensure that a message ciphered by a generated private key can be deciphered by the associated public key.
    BOOST_AUTO_TEST_CASE(PrivateCipher_PublicDecipher) {
        // Used to obtain a seed for the RNG.
        std::random_device rd;
        // RNG: Standard Mersenne twister engine.
        std::mt19937 rng(rd());
        // Generate a random set of keys to run the test with.
        const auto keys = KeyPair::generate(rng);

        // Generate a random message of arbitrary size (2048) to cipher.
        const auto source = randomMessage(2048);

        // Create a container for the ciphered data.
        Message<Ciphered> ciphered;
        // And cipher the message into the container using the private key.
        keys.pri.cipher(source, ciphered);

        // Ensure that the ciphered message contain the same amount of elements (!=bytes).
        BOOST_CHECK_EQUAL(ciphered.size, source.size);

        // Create a container for the deciphered data.
        Message<Clear> deciphered;
        // And decipher the message into the container using the public key.
        keys.pub.decipher(ciphered, deciphered);

        // Ensure that the deciphered message matches the source.
        BOOST_CHECK(strcmp(source.data, deciphered.data));
    }

    /// Ensure that a message ciphered by a generated public key can be deciphered by the associated private key.
    BOOST_AUTO_TEST_CASE(PublicCipher_PrivateDecipher) {
        // Used to obtain a seed for the RNG.
        std::random_device rd;
        // RNG: Standard Mersenne twister engine.
        std::mt19937 rng(rd());

        // Generate a random set of keys to run the test with.
        const auto keys = KeyPair::generate(rng);

        // Generate a random message of arbitrary size (2048) to cipher.
        const auto source = randomMessage(2048);

        // Create a container for the ciphered data.
        Message<Ciphered> ciphered;
        // And cipher the message into the container using the public key.
        keys.pub.cipher(source, ciphered);

        // Ensure that the ciphered message contain the same amount of elements (!=bytes).
        BOOST_CHECK_EQUAL(ciphered.size, source.size);

        // Create a container for the deciphered data.
        Message<Clear> deciphered;
        // And decipher the message into the container using the private key.
        keys.pri.decipher(ciphered, deciphered);

        // Ensure that the deciphered message matches the source.
        BOOST_CHECK(strcmp(source.data, deciphered.data));
    }
}
