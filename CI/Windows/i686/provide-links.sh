# @Authors: Axel Paccalin,
# @Brief: Copy the dynamically linked libraries to the build directory.

ENV_NAME="i686-w64-mingw32"
GCC_V=$(ls -1 "/usr/lib/gcc/$ENV_NAME/" | grep "posix" | sed -n '$p')

cp "/usr/lib/gcc/$ENV_NAME/$GCC_V/libstdc++-6.dll" build/
cp "/usr/lib/gcc/$ENV_NAME/$GCC_V/libgcc_s_sjlj-1.dll" build/
cp "/usr/$ENV_NAME/lib/libwinpthread-1.dll" build/
