/** @file    Key.hpp
 *  @authors andrewkiluk (C base), Axel Paccalin (C++/OOP, rng, doc)
 *  @brief   Declaration of the 64bit RSA ciphering / deciphering algorithm and some related files IO functions.
 *  @details Both the ciphering / deciphering algorithms and the random key generation where ported from the C project https://github.com/andrewkiluk/RSA-Library/blob/master/rsa.h
 *           In addition to the port to C++ & OOP, the random key generation was upgraded to C++11 security standards
 * */

#ifndef CPP_RSA_KEY_HPP
#define CPP_RSA_KEY_HPP

#include <random>
#include "Message.hpp"

namespace CPP_RSA{
    /** @class   Key
     *  @brief   Class storing the two component of a 64bit RSA key, and providing ciphering / deciphering methods.
     *  @details Both the modulus and the exponent are needed to cipher / decipher a message.
     *           The same key cannot both cipher AND decipher the same message, to decipher the message ciphered with a key, the paired key is required (see KeyPair).
     *           Methods to handle IO with files are also available.
     * */
    class Key{
    private:
        long long modulus,   //!< 64 bit modulus part of the key.
                  exponent;  //!< 64 bit exponent part of the key.

        /** @brief   Empty constructor.
         *  @details The empty constructor is private.
         *           Due to the fact that the data members are considered immutable after initialisation,
         *           it is only used for later affectation from the class static methods, thus still accessible, even in a private scope.
         * */
        Key() = default;
    public:
        /** @brief   Parametric constructor.
         *  @details It is mostly intended for testing purposes, but could come in handy in en unexpected use case.
         *  @param   modulus The modulus of the key.
         *  @param   exponent The exponent of the key.
         * */
        Key(const long long &modulus, const long long &exponent);
        /** @brief Copy constructor.
         *  @param other The Key to copy.
         * */
        Key(const Key &other) = default;

        /** @brief modulus data member accessor.
         * */
        const long long int &getModulus() const;
        /** @brief exponent data member accessor.
         * */
        const long long int &getExponent() const;

        /** @brief Cipher a message.
         *  @details Compute the ciphered version of a message according to the RSA algorithm with the current key.
         *  @param clear The clear Message to cypher.
         *  @param ciphered The ciphered Message container into which the result will be written.
         * */
        void cipher(const Message<Clear> &clear, Message<Ciphered> &ciphered) const;
        /** @brief Decipher a message.
         *  @details Compute the ciphered version of a message according to the RSA algorithm with the current key.
         *  @param ciphered The ciphered Message to decypher.
         *  @param clear The clear Message container into which the result will be written.
         * */
        void decipher(const Message<Ciphered> &ciphered, Message<Clear> &clear) const;

        /** @brief   Save a Key to a file.
         *  @warning The creation of non-existing sub-directories is not handled yet, and will result in an exception.
         *  @param   path The file path/file.ext relative to the current working directory into which Key will be saved.
         * */
        void save(const std::string &path) const;

        /** @brief   Load a Key from a file.
         *  @warning An invalid (non existing) file path or a parsing error will result in an exception.
         *  @param   path The file path/name.ext relative to the current working directory from which the Key will be loaded.
         * */
        static Key load(const std::string &path);
    };

    /** @struct  KeyPair
     *  @brief   Container storing a pair of private and public key.
     *  @details A key can decipher any message ciphered by the other.
     *           A static function with parametric rng can generate new pairs.
     *           Methods to handle IO with files are also available.
     * */
    struct KeyPair{
        Key pri,  //!< Private Key.
            pub;  //!< Public Key.

        /** @brief   Save a KeyPair to a pair of file.
         *  @details The private Key and public Key will respectively be saved under '${path}.rsa-priv' and '${path}.rsa-pub'.
         *  @warning The creation of non-existing sub-directories is not handled yet, and will result in an exception.
         *  @param   path The file template path/file.* relative to the current working directory under which both Key objects will be saved.
         * */
        void save(const std::string &path) const;
        /** @brief   Load a KeyPair from a pair of file.
         *  @details The private Key and public Key will respectively be loaded from '${path}.rsa-priv' and '${path}.rsa-pub'.
         *  @warning The creation of non-existing sub-directories is not handled yet, and will result in an exception.
         *  @param   path The file template path/file.* relative to the current working directory under which both Key objects will be saved.
         * */
        static KeyPair load(const std::string &path);

        /** @brief   Generate a new random KeyPair.
         *  @details The RNG can be seeded to produce deterministic behavior'.
         *  @param   rng The random number generator used to produce randomness in the KeyPair.
         * */
        static KeyPair generate(std::mt19937 &rng);
    };
}

#endif //CPP_RSA_KEY_HPP
