/** @file    Key.cpp
 *  @authors andrewkiluk (C base), oskarvonephesos (overflow protection), Axel Paccalin (C++ & OOP, rng, deciphering optimisations, doc)
 *  @brief   Implementation of the 64bit RSA ciphering / deciphering algorithm and some related files IO functions.
 *  @details Both the ciphering / deciphering algorithms and the random key generation where ported from the C project https://github.com/andrewkiluk/RSA-Library/blob/master/rsa.c
 *           In addition to the port to C++ & OOP, the random key generation was upgraded to C++11 security standards, and the algorithms were optimized.
 * */

#include <fstream>
#include "Key.hpp"
#include "PrimeList.hpp"

using std::string;                                  // Container
using std::swap;                                    // Swap variables
using std::ofstream, std::ifstream, std::ios;       // IO
using std::mt19937, std::uniform_int_distribution;  // RNG
using std::runtime_error;                           // Errors
using namespace CPP_RSA;                            // Lib cpp-rsa



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                               //
// Some math stuff: used in key-pair generation, and in ciphering / deciphering algorithms.                      //
//                                                                                                               //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// Greatest Common Divider.
long long gcd(long long a, long long b)
{
    while ( a != 0 ) {
        b %= a;
        swap(a, b);
    }
    return b;
}

/// Extended Euclidean Algorithm.
long long extEuclid(long long a, long long b)
{
    long long x = 0,
              y = 1,
              u = 1,
              v = 0;

    while (a!=0) {
        const long long q = b/a;

        b %= a;
        swap(a, b);

        x -= u*q;
        swap(x, u);

        y -= v*q;
        swap(y, v);
    }
    return y;
}

/// Multiplication followed by modulus `(a*b)%m` with overflow management.
long long modMult(long long a,long long b, long long m){
    // This is necessary since we will be dividing by a.
    if (a == 0 ) return 0;
    long long product = a * b;

    // If the multiplication does not overflow, we can directly use it.
    if (product / a == b) return product % m;

    // If a % 2 == 1 i. e. a >> 1 is not a / 2.
    if (a & 1) {
        product = modMult((a>>1), b, m);
        if ((product << 1) > product ) return ((( product << 1 ) % m ) + b) % m;
    }

    // Implicit merged else.
    product = modMult((a >> 1), b, m);
    if ((product << 1) > product) return (product << 1) % m;

    // Implicit else:
    // This is about 10x slower than the code above, but it will not overflow.
    long long sum = 0;

    while(b > 0) {
        if(b & 1) sum = (sum + a) % m;
        a = (2*a) % m;
        b>>=1;
    }

    return sum;
}
/// Exponentiation followed by modulus `(a^b)%m` with overflow management.
long long modExp(long long a, long long b, const long long &m)
{
    long long product = 1;

    if (a < 0 || b < 0 || m <= 0) return -1;
    a = a % m;
    while (b > 0){
        if (b & 1) product = modMult(product, a, m);
        a = modMult(a, a, m);
        b >>= 1;
    }

    return product;
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                               //
// Key methods, including: ciphering / deciphering algorithms and file saving / loading.                         //
//                                                                                                               //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/// Parametric constructor.
Key::Key(const long long &modulus,const long long &exponent)
: modulus(modulus), exponent(exponent) {}

/// Modulus member accessor.
const long long int &Key::getModulus() const {
    return modulus;
}
/// Exponent member accessor.
const long long int &Key::getExponent() const {
    return exponent;
}
/// RSA ciphering algorithm.
void Key::cipher(const Message<Clear> &clear, Message<Ciphered> &ciphered) const {
    ciphered.allocate(clear.size);

    for(long long i = 0; i < clear.size; i++)
        if ((ciphered.data[i] = modExp(clear.data[i], exponent, modulus)) == -1)
            throw runtime_error("Unexpected error while ciphering message");

}
/// RSA deciphering algorithm.
void Key::decipher(const Message<Ciphered> &ciphered, Message<Clear> &clear) const {
    clear.allocate(ciphered.size);

    // Go through each 8-byte chunk and decipher it.
    for(long long i = 0; i < ciphered.size; i++) clear.data[i] = modExp(ciphered.data[i], exponent, modulus);
}

/// Save a key to a file.
void Key::save(const string &path) const {
    ofstream o(path, ios::out | ios::binary);
    if(!o) throw runtime_error("Couldn't open file");

    o.write((char *) &modulus, sizeof(long long));
    o.write((char *) &exponent, sizeof(long long));
}
/// Load a key from a file.
Key Key::load(const string &path) {
    ifstream i(path, ios::in | ios::binary);
    if(!i) throw runtime_error("Couldn't open file");

    Key res;
    i.read((char *) &res.modulus, sizeof(long long));
    i.read((char *) &res.exponent, sizeof(long long));

    return res;
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                               //
// Key-pair methods, including: generation and simultaneous saving / loading of pairs of files.                  //
//                                                                                                               //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// Generate a new pair of keys.
KeyPair KeyPair::generate(mt19937 &rng) {
    // Two random primes that we will pick from the list.
    long long p = 0;
    long long q = 0;

    // Values of e should be sufficiently large to protect against naive attacks.
    long long e = (2 << 16) +1;
    long long d = 0;
    long long max = 0;
    long long phi_max = 0;

    // Uniform index distribution on the prime list.
    uniform_int_distribution<> primeIndexPicker(0, primeListSize-1);

    do{
        // Pick two random primes from the prime list
        p = primeList[primeIndexPicker(rng)];
        q = primeList[primeIndexPicker(rng)];

        max = p*q;
        phi_max = (p-1)*(q-1);
    }
    while(!(p && q) || (p == q) || (gcd(phi_max, e) != 1));

    // Next, we need to choose a,b, so that a*max+b*e = gcd(max,e). We actually only need b
    // here, and in keeping with the usual notation of RSA we'll call it d. We'd also like
    // to make sure we get a representation of d as positive, hence the while loop.
    d = extEuclid(phi_max,e);
    while(d < 0) d += phi_max;

    return {{max, d},
            {max, e}};
}

/// Save a pair of keys to a standardized pair of files.
void KeyPair::save(const string &path) const {
    pri.save(path + ".rsa-priv");
    pub.save(path + ".rsa-pub");
}
/// Load a pair of keys from a standardized pair of files.
KeyPair KeyPair::load(const string &path) {
    return {Key::load(path + ".rsa-priv"),
            Key::load(path + ".rsa-pub")};
}
