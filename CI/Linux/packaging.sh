# @Authors: Axel Paccalin,
# @Brief: A .deb package generation routing. Expecting build artibacts to be presents, and various env variables to be set by the CI.


# Create library root.
mkdir $LIBRARY_NAME

# Create a directory in which we will later put the .deb control file.
mkdir $LIBRARY_NAME/DEBIAN

# Copy the licence.
mkdir -p $LIBRARY_NAME/usr/share/doc/cpp-rsa
cp LICENCE $LIBRARY_NAME/usr/share/doc/cpp-rsa/copyright

# Copy the include (usually only for the -dev lib).
mkdir -p $LIBRARY_NAME/usr/include
cp -r include/cpp-rsa $LIBRARY_NAME/usr/include

# Copy the shared objects.
mkdir -p $LIBRARY_NAME/usr/lib
cp build/*.so $LIBRARY_NAME/usr/lib/

# Copy the executable.
mkdir -p $LIBRARY_NAME/usr/bin
cp build/cpp-rsa $LIBRARY_NAME/usr/bin/

# Compute the disk size of the package.
export PACKAGE_SIZE=`du -sh ${LIBRARY_NAME} | cut -f -1`

# Write the control file with all the proper variables.
cat CI/Linux/deb-pkg.control | eval "echo \"$(cat $1)\"" > $LIBRARY_NAME/DEBIAN/control

# Package the library, and put the target architecture in the file name.
dpkg-deb --build $LIBRARY_NAME
mv ./${LIBRARY_NAME}.deb ./${LIBRARY_NAME}_${PACKAGE_ARCHITECTURE}.deb

# Clean.
rm -rf $LIBRARY_NAME
