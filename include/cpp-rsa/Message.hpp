/** @file    Message.hpp
 *  @authors Axel Paccalin
 *  @brief   Message structure for storing both clear and ciphered data.
 * */

#ifndef CPP_RSA_MESSAGE_HPP
#define CPP_RSA_MESSAGE_HPP

#include <cstring>

namespace CPP_RSA{
    using Clear    = char;       //!< Type contained in clear messages     (8bit).
    using Ciphered = long long;  //!< Type contained in ciphered messages (64bit).

    template<typename T>
    /** @struct  Message
     *  @brief   Message structure for storing both clear and ciphered data.
     *  @details Although the template isn't constrained, the structure is only meant to work with the Clear and Ciphered types.
     * */
    struct Message {
        size_t size;  //!< Size of the data container.
        T *data;      //!< Data container.

        /** @brief   Empty constructor.
         *  @details Creates a message of size 0 with no data.
         * */
        Message()
        : size(0),         // Set size member to 0.
          data(nullptr){}  // Set the data pointer to null.

        /** @brief   Sized constructor.
         *  @details Creates an uninitialized message with a specific amount of memory allocated.
         *  @param   size The container size worth of memory to allocate.
         * */
        explicit Message(const size_t &size)
        : size(size),          // Initialize size member.
          data(new T[size]){}  // And allocate memory.

        /** @brief Parametric constructor.
         *  @param data The data to copy.
         *  @param size The size of the data to copy.
         * */
        Message(const T data[], const size_t &size)
        : Message(size) {                                // Initialize size member, allocate memory.
            memcpy(this->data, data, size * sizeof(T));  // And copy data.
        }

        /** @brief   Copy constructor.
         *  @param   other The other Message to copy.
         *  @warning Costly O(n) operation. Consider working on a reference or a pointer.
         * */
        Message(const Message<T> &other) : Message(other.data, other.size) {}  // Copy using parametric constructor.

        /** @brief   Destructor.
         *  @details Frees any memory allocated during lifetime.
         * */
        virtual ~Message(){
            delete[] data;  // Free the allocated memory.
        }

        /** @brief   (Re-)Allocation member function.
         *  @details Frees any existing allocation and creates a new uninitialized allocation of size 'size'.
         *  @param   size The container size worth of memory to allocate.
         * */
        void allocate(const size_t &size){
            delete[] data;       // Free the memory of the previous allocation.
            this->size = size;   // Update the size member.
            data = new T[size];  // Create new allocation.
        }
    };
}

#endif //CPP_RSA_MESSAGE_HPP
